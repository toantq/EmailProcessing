import os.path
import platform
from places import places
from trie import Trie
import re
from constant import patterns, removes, date, document_location
import googlemaps
import json
import pickle
from multiprocessing import Queue

def purify_word(word):
    word = word.lower().strip()
    if word == "½":
        return "half"
    if "tiễn" == word:
        return "tien"
    word = word.replace("\xc2\xa0", " ")
    s = '.-/:;!,& )<>?('
    while word[0] in s:
        word = word[1:]
    while word[-1] in s:
        word = word[:-1]
    return word


def find_place(data, trie):
    places = []
    i = 0
    while i < len(data):
        j = 0
        current_node = trie.head
        while True:
            try:
                word = data[i + j]
                word = purify_word(word)
            except IndexError:
                if current_node.data:
                    places.append(current_node.place)
                break
            if word in current_node.children:
                current_node = current_node.children[word]
                j += 1
            else:
                if current_node.data:
                    places.append(current_node.place)
                    i += len(current_node.data.split()) - 1
                break
        i += 1
    return places


def purify_data(data):
    for r in removes:
        data = data.replace(r, ' ')
    for pattern in patterns:
        if ' ' in pattern:
            results = re.findall(pattern, data, re.IGNORECASE)
            for pt in results:
                pt = ''.join(pt).strip()
                data = data.replace(pt, pt.replace(' ', ''))
    data = data.split()
    return data


def find_distance(places):
    distance = 0.0
    gmaps = googlemaps.Client(key='AIzaSyAyk2O1iYtP5Ow5J6jiFUHyiVPuklZUj9U')
    if len(places) >= 1:
        for i in range(len(places) - 1):
            json_string = str(gmaps.distance_matrix(places[i], places[i + 1]))
            json_string = json_string.replace("'", '"')
            data = json.loads(json_string)
            if data['rows'][0]['elements'][0]['status'] == 'OK':
                distance += int(data['rows'][0]['elements'][0]['distance']['value'])
            elif places[i].strip() == 'City Tour' or places[i + 1].strip() == 'City Tour':
                distance += 100000
            elif places[i].strip() == 'Half City Tour' or places[i + 1].strip() == 'Half City Tour':
                distance += 50000
    return distance / 1000


def process_text(trie, data):
    results = []
    places = []
    data = purify_data(data)
    for p in patterns:
        pattern = re.compile(p, re.IGNORECASE)
        index = [(i, word) for i, word in enumerate(data) if pattern.match(data[i])]
        if len(index) == 0:
            continue
        for i in range(len(index) - 1):
            places.append(([index[i][1]], find_place(data[index[i][0]:index[i + 1][0]], trie)))
        places.append(([index[i + 1][1]], find_place(data[index[i + 1][0]:], trie)))
        for item in places:
            if len(item[1]) == 0:
                continue
            results.append(date + str(item[0][0]) + '\n')
            for place in item[1]:
                if "tiễn sân bay" == place:
                    results.append("Hà Nội\n")
                    results.append("Nội Bài\n")
                elif "đón sân bay" == place:
                    results.append("Hà Nội\n")
                    results.append("Nội Bài\n")
                    results.append("Hà Nội\n")
                else:
                    results.append(place + '\n')
    return results


def start_app():
    trie = Trie()
    if os.path.exists(document_location):
        with open(document_location, 'rb') as f:
            trie = pickle.load(f)

    for place in places[trie.last_position:]:
        trie.add(place[0].strip(), place[1].strip())
        trie.last_position += 1

    with open(document_location, 'wb') as f:
        pickle.dump(trie, f, pickle.HIGHEST_PROTOCOL)

    return trie









