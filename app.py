import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtWebKit import QWebView
import utils
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
from constant import title, date, distance_string, font_location, document_folder
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont


class Window(QWidget):

    def __init__(self):
        super(Window, self).__init__()

        self.open_file_btn = QPushButton("Mở tệp", self)
        self.process_btn = QPushButton("Tính khoảng cách", self)
        self.places_btn = QPushButton("Tìm địa điểm")

        self.browser = QTextEdit()
        self.list_view = QTreeView(self)
        self.splitter = QSplitter(Qt.Horizontal)
        self.layout = QHBoxLayout()

        self.left_layout = QGridLayout()
        self.right_layout = QGridLayout()

        self.left_widget = QWidget()
        self.right_widget = QWidget()

        self.left_widget.setLayout(self.left_layout)
        self.left_widget.setStyleSheet("background-color: rgb(255, 255, 255); border:1px solid rgb(0, 0, 0); ")
        self.right_widget.setLayout(self.right_layout)
        self.right_widget.setStyleSheet("background-color: rgb(255, 255, 255); border:1px solid rgb(0, 0, 0); ")

        self.splitter.addWidget(self.left_widget)
        self.splitter.addWidget(self.right_widget)

        self.left_layout.addWidget(self.browser, 1, 1, 1, 3)
        self.left_layout.addWidget(self.open_file_btn, 2, 1, 1, 1)
        self.left_layout.addWidget(self.process_btn, 2, 2, 1, 1)
        self.left_layout.addWidget(self.places_btn, 2, 3, 1, 1)

        self.right_layout.addWidget(self.list_view, 1, 1, 1, 3)

        self.layout.addWidget(self.splitter)
        self.setLayout(self.layout)

        self.open_file_btn.clicked.connect(self.open_button_handler)
        self.process_btn.clicked.connect(self.process_button_handler)
        self.places_btn.clicked.connect(self.places_btn_handler)

        self.setWindowTitle("Xử lý email")
        self.results = ''

        self.list_model = None
        self.select_all_cb = None
        self.old_list_model = None

        self.back_btn = None
        self.pdf_btn = None
        self.edit_view = None
        self.trie = utils.start_app()

        self.list_of_item = []
        self.output = ''

        pdfmetrics.registerFont(TTFont('times', str(font_location) + 'times.ttf'))
        pdfmetrics.registerFont(TTFont('times-bold', str(font_location) + 'timesbd.ttf'))
        pdfmetrics.registerFont(TTFont('times-italic', str(font_location) + 'timesi.ttf'))
        pdfmetrics.registerFont(TTFont('times-bold-italic', str(font_location) + 'timesbi.ttf'))

    def back_button_handler(self):

        self.left_layout.removeWidget(self.back_btn)
        self.back_btn.deleteLater()
        self.back_btn = None

        self.left_layout.removeWidget(self.pdf_btn)
        self.pdf_btn.deleteLater()
        self.pdf_btn = None

        self.left_layout.removeWidget(self.edit_view)
        self.edit_view.deleteLater()
        self.edit_view = None

        if self.old_list_model:
            self.list_model = self.old_list_model
            self.list_view.setModel(self.list_model)
            self.list_view.show()

            self.process_btn = QPushButton("Tính khoảng cách", self)
            self.process_btn.clicked.connect(self.process_button_handler)
            self.left_layout.addWidget(self.process_btn, 2, 2, 1, 1)

            self.open_file_btn = QPushButton("Mở tệp", self)
            self.open_file_btn.clicked.connect(self.open_button_handler)
            self.left_layout.addWidget(self.open_file_btn, 2, 1, 1, 1)

            self.places_btn = QPushButton("Tìm địa điểm", self)
            self.places_btn.clicked.connect(self.places_btn_handler)
            self.left_layout.addWidget(self.places_btn, 2, 3, 1, 1)

    def pdf_button_handler(self):
        file_name = QFileDialog.getSaveFileName(self, "Lưu PDF", "", ".pdf")
        c = canvas.Canvas(file_name, pagesize=letter)
        text = self.output
        lines = text.split('\n')

        c.setFont('times-bold', 15)
        c.drawString(250, 700, "LỊCH TRÌNH DỰ KIẾN")

        y = 670
        for line in lines:
            if date in line:
                c.setFont('times-bold-italic', 10)
                c.drawString(50, y, line)
            else:
                c.setFont('times', 10)
                c.drawString(70, y, line)
            y -= 20
        c.save()

    def select_all(self):
        model = self.list_model
        if self.select_all_cb.checkState():
            for index in range(len(self.list_of_item)):
                item = self.list_of_item[index]
                if item.isCheckable():
                    item.setCheckState(Qt.Checked)

        for index in range(len(self.list_of_item)):
            item = self.list_of_item[index]
            if date in item.text() and item.checkState():
                j = index + 1
                while j < len(range(len(self.list_of_item))):
                    item1 = self.list_of_item[j]
                    if date not in item1.text():
                        item1.setCheckState(Qt.Checked)
                    else:
                        break
                    j += 1


    def places_btn_handler(self, results):
        
        self.list_model = QStandardItemModel(self.list_view)
        self.list_model.setHorizontalHeaderLabels(["Danh sách địa điểm"])

        self.list_of_item = []

        results = utils.process_text(self.trie, self.browser.toPlainText())
        self.select_all_cb = QStandardItem(title)
        self.select_all_cb.setCheckable(True)
        self.list_model.itemChanged.connect(self.select_all)
        self.list_model.appendRow(self.select_all_cb)
        self.list_model.itemChanged.connect(self.select_all)

        self.list_of_item.append(self.select_all_cb)

        list_of_id = []
        for i, word in enumerate(results):
            if date in word:
                list_of_id.append(i)

        for idx in range(len(list_of_id) - 1):
            parent = QStandardItem(results[list_of_id[idx]])
            parent.setCheckable(True)
            self.list_model.appendRow(parent)
            self.list_of_item.append(parent)
            for j in range(list_of_id[idx] + 1,list_of_id[idx + 1]):
                child = QStandardItem(results[j])
                child.setCheckable(True)
                parent.appendRow(child)
                self.list_of_item.append(child)

        parent = QStandardItem(results[list_of_id[idx + 1]])
        parent.setCheckable(True)
        self.list_model.appendRow(parent)
        self.list_of_item.append(parent)
        for j in results[list_of_id[idx + 1] + 1:]:
            child = QStandardItem(j)
            child.setCheckable(True)
            parent.appendRow(child)
            self.list_of_item.append(child)

        self.list_view.setModel(self.list_model)
        self.list_view.show()


    def open_button_handler(self):
        file_name = QFileDialog.getOpenFileName(w, 'Mở tệp', document_folder)
        with open(file_name, 'r', encoding="utf8") as f:
            self.browser.setText(f.read())
        self.browser.show()


    def show_dialog(self):
        QMessageBox.critical(w, "Chú ý", "Bạn phải chọn 2 địa điểm trở lên để tính khoảng cách")


    def process_button_handler(self):

        self.back_btn = QPushButton("Quay về", self)
        self.pdf_btn = QPushButton("Xuất ra PDF", self)
        self.edit_view = QTextEdit("", self)

        self.back_btn.clicked.connect(self.back_button_handler)
        self.pdf_btn.clicked.connect(self.pdf_button_handler)

        self.old_list_model = self.list_model
        i = 0
        set_of_items = []
        places = []

        while self.list_of_item[i]:
            if date in self.list_of_item[i].text():
                if i > 0:
                    if len(places) >= 2:
                        distance = int(utils.find_distance(places[1:]))
                        set_of_items.append((places, distance))
                places = [self.list_of_item[i].text(), ]

            if self.list_of_item[i].checkState() and date not in self.list_of_item[i].text():
                places.append(self.list_of_item[i].text())
            i += 1

            if i == len(self.list_of_item):
                if len(places) >= 2:
                    distance = int(utils.find_distance(places[1:]))
                    set_of_items.append((places, distance))
                break

        results = ''
        self.output = ''

        if len(set_of_items) > 0:
            for si in set_of_items:
                results += 'HÀNH TRÌNH '
                self.output += 'HÀNH TRÌNH '
                for i, p in enumerate(si[0]):
                    if i != len(si[0]) - 1:
                        if date not in str(p):
                            results += str(p).replace('\n', ' ---> ')
                            self.output += '+ ' + str(p)
                        else:
                            results += str(p)
                            self.output += str(p)
                    else:
                        results += str(p)
                results += distance_string + str(si[1]) + ' km' + '\n'
                self.output += distance_string + str(si[1]) + ' km' + '\n'

            self.edit_view.setText(results)

            self.left_layout.removeWidget(self.process_btn)
            self.process_btn.deleteLater()
            self.process_btn = None

            self.left_layout.removeWidget(self.open_file_btn)
            self.open_file_btn.deleteLater()
            self.open_file_btn = None

            self.left_layout.removeWidget(self.places_btn)
            self.places_btn.deleteLater()
            self.places_btn = None            

            self.left_layout.addWidget(self.back_btn, 2, 1, 1, 1)
            self.left_layout.addWidget(self.pdf_btn, 2, 2, 1, 1)

            self.right_layout.addWidget(self.edit_view, 1, 1, 1, 3)
        else:
            self.show_dialog()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Window()
    w.showMaximized()
    sys.exit(app.exec_())
