from cx_Freeze import setup, Executable
import sys
import requests.certs

buildOptions = dict(
    packages=['googlemaps', 'idna', 'urllib', 'requests']
)

base = None
if sys.platform == "win32":
    base = "Win32GUI"

executables = [
    Executable(
        'app.py',
        base=base,
        targetName='Email Processing',
    )
]

setup(
    name='Email Processing App',
    version='0.1',
    desription='A program for email processing',
    options=dict(build_exe=buildOptions),
    executables=executables
)

