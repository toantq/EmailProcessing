import platform
import os.path
from PyQt4.QtGui import QDesktopServices

month = '(?:jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)'
patterns = [
r'[0-9]+/[0-9]+(/[0-9]+)?',
r"\d+[ ]*%s[ ]*\d+|\d+[ ]*%s[ ]*\d*|\d*[ ]*%s[ ]*\d+" % (month, month, month),
]
removes = [':', '<', '>', '/strong', '/span', '/p', '/td', '/tr', '/tbody', '/table']
title = "CHỌN TẤT CẢ"
date = "NGÀY: "
distance_string = "KHOẢNG CÁCH DỰ KIẾN: "

document_location= str(os.path.expanduser('~')) 
document_folder = document_location

if platform.system() == "Windows":  
    font_location = "C:\\Windows\\Fonts\\"
    document_location += '\Documents\EmailProcessingData\places.pkl'
    document_folder += '\Documents'
elif platform.system() == "Linux":
    font_location = "/usr/share/fonts/truetype/msttcorefonts/"
    document_location += '/Documents/EmailProcessingData/places.pkl'
    document_folder += '/Documents'

if not os.path.exists(document_location[:-10]):
    os.mkdir(document_location[:-10])


